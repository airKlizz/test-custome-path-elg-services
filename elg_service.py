from elg import FlaskService
from elg.model import TextsResponse

from loguru import logger

class TranslateService(FlaskService):
    def process_text(self, req):
        translation = f"translate: '{req.content}' from {self.url_param('src')} to {self.url_param('target')}"
        logger.debug(translation)
        return TextsResponse(texts=[{"content": translation}])

service = TranslateService("Translate", path = "/translate/<src>/<target>")
app = service.app
